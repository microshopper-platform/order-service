package com.microshopper.orderservice.model;

public enum Status {

  REQUESTED,

  IN_PROGRESS,

  TRACEABLE,

  COMPLETED,

  CANCELED,
}
