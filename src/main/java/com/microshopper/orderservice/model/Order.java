package com.microshopper.orderservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Document(collection = "order")
public class Order {

  @MongoId
  @Field("_id")
  private String id = UUID.randomUUID().toString();

  @Field("products")
  private List<Product> products;

  @Field("username")
  private String username;

  @Field("manufacturer_id")
  private Long manufacturerId;

  @Field("status")
  private Status status = Status.REQUESTED;

  @Field("transporter_assignee")
  private String transporterAssignee;
}
