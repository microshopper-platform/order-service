package com.microshopper.orderservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {

  private Long productId;

  private Integer price;

  private Integer quantity;
}
