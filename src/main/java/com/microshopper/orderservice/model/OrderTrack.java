package com.microshopper.orderservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.UUID;

@Getter
@Setter
@Document(collection = "order_track")
public class OrderTrack {

  @MongoId
  @Field("_id")
  private String id = UUID.randomUUID().toString();

  @Field("order_id")
  private String orderId;

  @Field("latitude")
  private Double latitude;

  @Field("longitude")
  private Double longitude;
}
