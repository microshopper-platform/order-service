package com.microshopper.orderservice.repository;

import com.microshopper.orderservice.model.Order;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface OrderRepository extends ReactiveMongoRepository<Order, String> {

  Flux<Order> getAllByUsername(String username);

  Flux<Order> getAllByManufacturerId(Long id);

  Flux<Order> getAllByTransporterAssignee(String transporterAssignee);
}
