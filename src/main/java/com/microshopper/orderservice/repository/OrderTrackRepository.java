package com.microshopper.orderservice.repository;

import com.microshopper.orderservice.model.OrderTrack;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface OrderTrackRepository extends ReactiveMongoRepository<OrderTrack, String> {

  Mono<OrderTrack> getByOrderId(String orderId);
}
