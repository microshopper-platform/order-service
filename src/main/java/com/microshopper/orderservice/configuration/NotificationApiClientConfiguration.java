package com.microshopper.orderservice.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microshopper.notificationservice.web.api.NotificationApi;
import com.microshopper.notificationservice.web.restclient.ApiClient;
import com.microshopper.notificationservice.web.restclient.RFC3339DateFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

import java.text.DateFormat;
import java.util.TimeZone;

@Configuration
public class NotificationApiClientConfiguration {

  private final WebClient.Builder webClientBuilder;
  private final ObjectMapper objectMapper;

  @Autowired
  public NotificationApiClientConfiguration(
    WebClient.Builder webClientBuilder,
    ObjectMapper objectMapper) {

    this.webClientBuilder = webClientBuilder;
    this.objectMapper = objectMapper;
  }

  @Bean
  public ApiClient notificationApiClient() {

    DateFormat dateFormat = new RFC3339DateFormat();
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

    ApiClient apiClient = new ApiClient(webClientBuilder.build(), objectMapper, dateFormat);
    apiClient.setBasePath("http://notification-service");

    return apiClient;
  }

  @Bean
  public NotificationApi notificationApi() {
    return new NotificationApi(notificationApiClient());
  }
}
