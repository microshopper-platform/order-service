package com.microshopper.orderservice.configuration;

import com.microshopper.orderservice.repository.OrderRepository;
import com.microshopper.orderservice.repository.OrderTrackRepository;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@EnableReactiveMongoRepositories(basePackageClasses = {
  OrderRepository.class,
  OrderTrackRepository.class})
public class MongoReactiveConfiguration extends AbstractReactiveMongoConfiguration {

  private static final String DATABASE_NAME = "order_service_db";

  @Override
  @Bean
  public MongoClient reactiveMongoClient() {
    return MongoClients.create();
  }

  @Override
  protected String getDatabaseName() {
    return DATABASE_NAME;
  }
}
