package com.microshopper.orderservice.service;

import com.microshopper.orderservice.web.dto.OrderLocationDto;
import com.microshopper.orderservice.web.dto.UpdateOrderLocationDto;
import reactor.core.publisher.Mono;

public interface OrderTrackingService {

  void initiateOrderForTracking(String orderId, String username);

  Mono<OrderLocationDto> getOrderLocation(String orderId);

  void updateOrderLocation(String orderId, UpdateOrderLocationDto updateOrderLocationDto);
}
