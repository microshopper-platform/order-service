package com.microshopper.orderservice.service.impl;

import com.microshopper.notificationservice.web.api.NotificationApi;
import com.microshopper.notificationservice.web.dto.Notification;
import com.microshopper.orderservice.model.OrderTrack;
import com.microshopper.orderservice.repository.OrderTrackRepository;
import com.microshopper.orderservice.service.OrderTrackingService;
import com.microshopper.orderservice.web.dto.OrderLocationDto;
import com.microshopper.orderservice.web.dto.UpdateOrderLocationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class OrderTrackingServiceImpl implements OrderTrackingService {

  private final OrderTrackRepository orderTrackRepository;
  private final NotificationApi notificationApi;

  @Autowired
  public OrderTrackingServiceImpl(
    OrderTrackRepository orderTrackRepository,
    NotificationApi notificationApi) {

    this.orderTrackRepository = orderTrackRepository;
    this.notificationApi = notificationApi;
  }

  @Override
  public void initiateOrderForTracking(String orderId, String username) {

    OrderTrack orderTrack = new OrderTrack();
    orderTrack.setOrderId(orderId);

    orderTrackRepository
      .getByOrderId(orderId)
      .defaultIfEmpty(orderTrack)
      .map(ot -> orderTrackRepository.save(ot).subscribe())
      .subscribe(subscribe -> {
        String notificationTitle = "Your order is now traceable";
        String notificationMessage = String.format("Your order with id: %s is now traceable", orderId);
        notificationApi.sendPushNotifications(createNotification(notificationTitle, notificationMessage, username)).subscribe();
      });
  }

  @Override
  public Mono<OrderLocationDto> getOrderLocation(String orderId) {

    return orderTrackRepository
      .getByOrderId(orderId)
      .map(orderTrack -> {
        OrderLocationDto orderLocationDto = new OrderLocationDto();
        orderLocationDto.setLatitude(orderTrack.getLatitude());
        orderLocationDto.setLongitude(orderTrack.getLongitude());
        return orderLocationDto;
    });
  }

  @Override
  public void updateOrderLocation(String orderId, UpdateOrderLocationDto updateOrderLocationDto) {

    orderTrackRepository
      .getByOrderId(orderId)
      .flatMap(orderTrack -> {
      orderTrack.setLatitude(updateOrderLocationDto.getLatitude());
      orderTrack.setLongitude(updateOrderLocationDto.getLongitude());

      return orderTrackRepository.save(orderTrack);
    }).subscribe();
  }

  private Notification createNotification(String title, String message, String username) {
    Notification notification = new Notification();
    notification.setTitle(title);
    notification.setMessage(message);
    notification.setUsername(username);

    return notification;
  }
}
