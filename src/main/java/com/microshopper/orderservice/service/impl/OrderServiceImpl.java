package com.microshopper.orderservice.service.impl;

import com.microshopper.orderservice.mapper.OrderMapper;
import com.microshopper.orderservice.mapper.ProductMapper;
import com.microshopper.orderservice.model.Order;
import com.microshopper.orderservice.model.Product;
import com.microshopper.orderservice.model.Status;
import com.microshopper.orderservice.repository.OrderRepository;
import com.microshopper.orderservice.service.OrderService;
import com.microshopper.orderservice.service.OrderTrackingService;
import com.microshopper.orderservice.web.dto.NewOrderDto;
import com.microshopper.orderservice.web.dto.OrderDto;
import com.microshopper.orderservice.web.dto.ProductDto;
import com.microshopper.orderservice.web.dto.StatusDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;
  private final OrderMapper orderMapper;
  private final ProductMapper productMapper;
  private final OrderTrackingService orderTrackingService;

  @Autowired
  public OrderServiceImpl(
    OrderRepository orderRepository,
    OrderMapper orderMapper,
    ProductMapper productMapper,
    OrderTrackingService orderTrackingService) {

    this.orderRepository = orderRepository;
    this.orderMapper = orderMapper;
    this.productMapper = productMapper;
    this.orderTrackingService = orderTrackingService;
  }

  @Override
  public Mono<OrderDto> getOrderById(String id) {
    return orderRepository.findById(id).map(order -> {
      OrderDto orderDto = orderMapper.mapToOrderDto(order);
      List<ProductDto> productDtos = order.getProducts().stream()
        .map(productMapper::mapToProductDto)
        .collect(Collectors.toList());
      orderDto.setProducts(productDtos);
      return orderDto;
    });
  }

  @Override
  public Flux<OrderDto> getAllOrdersForUsername(String username) {
    return orderRepository.getAllByUsername(username).map(order -> {
      OrderDto orderDto = orderMapper.mapToOrderDto(order);
      List<ProductDto> productDtos = order.getProducts().stream()
        .map(productMapper::mapToProductDto)
        .collect(Collectors.toList());
      orderDto.setProducts(productDtos);
      return orderDto;
    });
  }

  @Override
  public Flux<OrderDto> getAllOrdersForManufacturerId(Long manufacturerId) {
    return orderRepository.getAllByManufacturerId(manufacturerId).map(order -> {
      OrderDto orderDto = orderMapper.mapToOrderDto(order);
      List<ProductDto> productDtos = order.getProducts().stream()
        .map(productMapper::mapToProductDto)
        .collect(Collectors.toList());
      orderDto.setProducts(productDtos);
      return orderDto;
    });
  }

  @Override
  public Flux<OrderDto> getAllOrdersForTransporterAssignee(String transporterAssignee) {
    return orderRepository.getAllByTransporterAssignee(transporterAssignee).map(order -> {
      OrderDto orderDto = orderMapper.mapToOrderDto(order);
      List<ProductDto> productDtos = order.getProducts().stream()
        .map(productMapper::mapToProductDto)
        .collect(Collectors.toList());
      orderDto.setProducts(productDtos);
      return orderDto;
    });
  }

  @Override
  public void addNewOrder(NewOrderDto newOrderDto) {

    Map<Long, List<Product>> manufacturersMap = new HashMap<>();
    String username = newOrderDto.getUsername();
    for (ProductDto productDto : newOrderDto.getProducts()) {
      Long manufacturerId = productDto.getManufacturerId();
      if (manufacturersMap.containsKey(manufacturerId)) {
        manufacturersMap.get(manufacturerId).add(productMapper.mapFromProductDto(productDto));
      } else {
        manufacturersMap.put(manufacturerId, Arrays.asList(productMapper.mapFromProductDto(productDto)));
      }
    }

    Flux.fromStream(manufacturersMap.entrySet().stream())
      .flatMap(entry -> {
        Order order = new Order();
        order.setManufacturerId(entry.getKey());
        order.setUsername(username);
        order.setProducts(entry.getValue());
        return orderRepository.save(order);
      })
      .subscribe();
  }

  @Override
  public void cancelOrder(String id) {

    orderRepository.findById(id)
      .flatMap(order -> {
        order.setStatus(Status.CANCELED);
        return orderRepository.save(order);
      })
      .subscribe();
  }

  @Override
  public void updateOrderStatus(String id, StatusDto statusDto) {

    orderRepository.findById(id)
      .flatMap(order -> {
        if (!order.getStatus().name().equals(statusDto.name())) {
          order.setStatus(Status.valueOf(statusDto.name()));
          if (StatusDto.TRACEABLE.equals(statusDto)) {
            orderTrackingService.initiateOrderForTracking(id, order.getUsername());
          }
          return orderRepository.save(order);
        }
        return Mono.just(order);
      })
      .subscribe();
  }

  @Override
  public void assignOrderTransporter(String id, String transporterAssignee) {
    orderRepository.findById(id)
      .flatMap(order -> {
        order.setTransporterAssignee(transporterAssignee);
        return orderRepository.save(order);
      })
      .subscribe();
  }
}
