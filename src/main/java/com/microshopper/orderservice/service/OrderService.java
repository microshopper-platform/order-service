package com.microshopper.orderservice.service;

import com.microshopper.orderservice.web.dto.NewOrderDto;
import com.microshopper.orderservice.web.dto.OrderDto;
import com.microshopper.orderservice.web.dto.StatusDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface OrderService {

  Mono<OrderDto> getOrderById(String id);

  Flux<OrderDto> getAllOrdersForUsername(String username);

  Flux<OrderDto> getAllOrdersForManufacturerId(Long manufacturerId);

  Flux<OrderDto> getAllOrdersForTransporterAssignee(String transporterAssignee);

  void addNewOrder(NewOrderDto newOrderDto);

  void cancelOrder(String id);

  void updateOrderStatus(String id, StatusDto statusDto);

  void assignOrderTransporter(String id, String transporterAssignee);
}
