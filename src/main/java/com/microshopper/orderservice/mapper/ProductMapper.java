package com.microshopper.orderservice.mapper;

import com.microshopper.orderservice.model.Product;
import com.microshopper.orderservice.web.dto.ProductDto;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

  public ProductDto mapToProductDto(Product product) {

    ProductDto productDto = new ProductDto();
    productDto.setProductId(product.getProductId());
    productDto.setPrice(product.getPrice());
    productDto.setQuantity(product.getQuantity());

    return productDto;
  }

  public Product mapFromProductDto(ProductDto productDto) {

    Product product = new Product();
    product.setProductId(productDto.getProductId());
    product.setPrice(productDto.getPrice());
    product.setQuantity(productDto.getQuantity());

    return product;
  }
}
