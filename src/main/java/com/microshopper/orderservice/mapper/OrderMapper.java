package com.microshopper.orderservice.mapper;

import com.microshopper.orderservice.model.Order;
import com.microshopper.orderservice.web.dto.OrderDto;
import com.microshopper.orderservice.web.dto.StatusDto;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper {

  public OrderDto mapToOrderDto(Order order) {

    OrderDto orderDto = new OrderDto();
    orderDto.setId(order.getId());
    orderDto.setUsername(order.getUsername());
    orderDto.setManufacturerId(order.getManufacturerId());
    orderDto.setStatus(StatusDto.valueOf(order.getStatus().name()));
    orderDto.setTransporterAssignee(order.getTransporterAssignee());

    return orderDto;
  }
}
