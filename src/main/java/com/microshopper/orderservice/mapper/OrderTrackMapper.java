package com.microshopper.orderservice.mapper;

import com.microshopper.orderservice.model.OrderTrack;
import com.microshopper.orderservice.web.dto.OrderTrackDto;
import org.springframework.stereotype.Component;

@Component
public class OrderTrackMapper {

  public OrderTrackDto mapToOrderTrackDto(OrderTrack orderTrack) {

    OrderTrackDto orderTrackDto = new OrderTrackDto();
    orderTrackDto.setId(orderTrack.getId());
    orderTrackDto.setOrderId(orderTrack.getOrderId());
    orderTrackDto.setLatitude(orderTrack.getLatitude());
    orderTrackDto.setLongitude(orderTrack.getLongitude());

    return orderTrackDto;
  }

  public OrderTrack mapFromOrderTrackDto(OrderTrackDto orderTrackDto) {

    OrderTrack orderTrack = new OrderTrack();
    orderTrack.setId(orderTrackDto.getOrderId());
    orderTrack.setLatitude(orderTrackDto.getLatitude());
    orderTrack.setLongitude(orderTrackDto.getLongitude());

    return orderTrack;
  }
}
