package com.microshopper.orderservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "UpdateOrderLocation")
public class UpdateOrderLocationDto extends LocationDto {
}
