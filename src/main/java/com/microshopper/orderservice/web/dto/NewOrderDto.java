package com.microshopper.orderservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;

import java.util.List;

@JsonRootName("NewOrder")
@Getter
public class NewOrderDto {

  private String username;

  private List<ProductDto> products;
}
