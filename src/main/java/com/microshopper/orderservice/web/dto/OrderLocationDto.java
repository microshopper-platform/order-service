package com.microshopper.orderservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "OrderLocation")
public class OrderLocationDto extends LocationDto {
}
