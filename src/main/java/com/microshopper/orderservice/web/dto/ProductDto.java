package com.microshopper.orderservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@JsonRootName(value = "Product")
@Getter
@Setter
public class ProductDto {

  private Long productId;

  private Long manufacturerId;

  private Integer price;

  private Integer quantity;
}
