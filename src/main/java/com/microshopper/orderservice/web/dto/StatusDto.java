package com.microshopper.orderservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("Status")
public enum StatusDto {

  REQUESTED,

  IN_PROGRESS,

  TRACEABLE,

  COMPLETED,

  CANCELED,
}
