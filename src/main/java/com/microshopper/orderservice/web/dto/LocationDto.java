package com.microshopper.orderservice.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class LocationDto {

  private Double latitude;

  private Double longitude;
}
