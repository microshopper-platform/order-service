package com.microshopper.orderservice.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonRootName(value = "Order")
@Getter
@Setter
public class OrderDto {

  private String id;

  @JsonIgnoreProperties(value = "manufacturerId")
  private List<ProductDto> products;

  private String username;

  private Long manufacturerId;

  private StatusDto status;

  private String transporterAssignee;
}
