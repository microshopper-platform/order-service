package com.microshopper.orderservice.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderTrackDto {

  private String id;

  private String orderId;

  private Double latitude;

  private Double longitude;
}
