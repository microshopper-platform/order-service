package com.microshopper.orderservice.web.controller;

import com.microshopper.orderservice.service.OrderService;
import com.microshopper.orderservice.service.OrderTrackingService;
import com.microshopper.orderservice.web.dto.NewOrderDto;
import com.microshopper.orderservice.web.dto.OrderDto;
import com.microshopper.orderservice.web.dto.OrderLocationDto;
import com.microshopper.orderservice.web.dto.StatusDto;
import com.microshopper.orderservice.web.dto.UpdateOrderLocationDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@Tag(name = "order", description = "The Order API")
public class OrderController {

  private static final String BASE_PATH = "/api/orders";

  private final OrderService orderService;
  private final OrderTrackingService orderTrackingService;

  public OrderController(
    OrderService orderService,
    OrderTrackingService orderTrackingService) {

    this.orderService = orderService;
    this.orderTrackingService = orderTrackingService;
  }

  @GetMapping(value = BASE_PATH + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public Mono<OrderDto> getOrderById(@PathVariable String id) {
    return orderService.getOrderById(id);
  }

  @GetMapping(value = BASE_PATH + "/account/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public Flux<OrderDto> getAllOrdersForUsername(@PathVariable String username) {
    return orderService.getAllOrdersForUsername(username);
  }

  @GetMapping(value = BASE_PATH + "/manufacturer/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public Flux<OrderDto> getAllOrdersForManufacturerId(@PathVariable Long id) {
    return orderService.getAllOrdersForManufacturerId(id);
  }

  @GetMapping(value = BASE_PATH + "/transporter/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public Flux<OrderDto> getAllOrdersForTransporterAssignee(@PathVariable String username) {
    return orderService.getAllOrdersForTransporterAssignee(username);
  }

  @PostMapping(value = BASE_PATH)
  @ResponseStatus(HttpStatus.CREATED)
  public void addNewOrder(@RequestBody NewOrderDto newOrderDto) {
    orderService.addNewOrder(newOrderDto);
  }

  @PatchMapping(value = BASE_PATH + "/cancel/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void cancelOrder(@PathVariable String id) {
    orderService.cancelOrder(id);
  }

  @PatchMapping(value = BASE_PATH + "/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void updateOrderStatus(@PathVariable String id, @RequestParam(name = "status") StatusDto statusDto) {
    orderService.updateOrderStatus(id, statusDto);
  }

  @GetMapping(value = BASE_PATH + "/track/{orderId}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  @ResponseStatus(HttpStatus.OK)
  public Flux<OrderLocationDto> getOrderLocation(@PathVariable String orderId) {
    return Flux.interval(Duration.ofSeconds(1)).flatMap(i -> orderTrackingService.getOrderLocation(orderId));
  }

  @PatchMapping(value = BASE_PATH + "/track/{orderId}")
  @ResponseStatus(HttpStatus.OK)
  public void updateOrderLocation(
    @PathVariable String orderId,
    @RequestBody UpdateOrderLocationDto updateOrderLocationDto) {

    orderTrackingService.updateOrderLocation(orderId, updateOrderLocationDto);
  }

  @PostMapping(value = BASE_PATH + "/{id}/assign-transporter")
  @ResponseStatus(HttpStatus.OK)
  public void assignOrderTransporter(@PathVariable String id, @RequestParam String transporterAssignee) {
    orderService.assignOrderTransporter(id, transporterAssignee);
  }
}
